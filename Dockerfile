FROM node:14

WORKDIR /app

# Копирование файла package.json и package-lock.json
COPY package*.json ./

# Установка зависимостей
RUN npm install

# Копирование исходного кода в контейнер
COPY . .

# Определение порта, на котором будет работать приложение
EXPOSE 3000

# Команда для запуска приложения
CMD [ "npm", "run", "start:server" ]