import Router from 'koa-router';
import defaultRoutes from './default';


const router = new Router({
    prefix: '/v1',
});

router.use(defaultRoutes);

export default router.routes();
