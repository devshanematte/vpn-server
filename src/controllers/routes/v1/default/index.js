import Router from 'koa-router';
import actions from './actions';

const router = new Router({
    prefix: '/tests',
});

router.get(
    '/test',
    actions.testMethod
);

export default router.routes();
