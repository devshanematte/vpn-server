
export default {
    testMethod: async (ctx) => {

        try {

            ctx.status = 200;
            return ctx.body = 'TEST';

        } catch (err) {
            ctx.status = 500;
            return (ctx.body = 'Error');
        }
    },
};
