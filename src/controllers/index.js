import Router from 'koa-router';

//routes
import routesApiV1 from './routes/v1';

const router = new Router();

router.use(routesApiV1);

export default router.routes();
